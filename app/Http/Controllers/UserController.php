<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\User;
use Mail;
use Validator;

class UserController extends Controller
{
    /**
     * Method to store user
     * User FormSubmissionRequest to validate form data
     * Mail to send mail
     */
	public function store(Request $request){
        //return $request::all();
        $validator = Validator::make($request->all(), [
            'g-recaptcha-response'=>'required',
            'email'=>'required|email|unique:users,email',
            'name'=>'required',
            'surname'=>'required',
            'number'=>'required',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors();
            $response = [
                'status' => 'error',
                'message' => $message,
            ];
            return response()->json($response, 409);
        } else {
            //Create and store user
            $user = new User();
            $user->name = $request->name;
            $user->surname = $request->surname;
            $user->email = $request->email;
            $user->number = $request->number;
            $user->save();

            //send mail to user and admin
            Mail::send(
                'email.userSubmission',
                [
                    'user' => $user,
                ],
                function (
                    $message
                ) use (
                    $user
                ) {
                    $message->to($user->email, $user->name);
                    $message->cc('admin@srsbsns.com', 'Admin');
                    $message->from('notifications@srsbsns.com', 'srsbsns');
                    $message->subject('New submission');
                }
            );
        }
        $response = [
            'status' => 'success',
            'message' => 'Submission successful',
        ];
        return response()->json($response);
    }

    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

}
