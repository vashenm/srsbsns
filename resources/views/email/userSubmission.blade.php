<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style type="text/css">
    *{margin:0; padding:0;}
    h1{
      font-size: 40px;
    }
    p{
      font-size: 20px;
    }
  </style>
</head>
<body>
  <div>
    <table style="margin:auto width:600px; border-collapse:collapse;"><!--containing table -->
     <!--body text-->
     <tr style="text-align: Center;background-color: #f2f2f2;font-weight: bold;padding-top: 30px;"> <!--name row-->
        <td style="padding:30px 50px 20px 50px;color: #000;"><h1>Hi <?php echo $user->name; ?>!</h1></td>
     </tr> <!-- END name row-->
     <tr style="text-align: center; background-color: #f2f2f2;"> <!--invite info row-->
        <td style="padding:0 50px 20px 50px;color: #000;">
            <p>Thank you for your submission.</p>
        </td>
        <td>
           <p>Name : <?php echo $user->name; ?></p> 
        </td>
        <td>
           <p>Surname : <?php echo $user->surname; ?></p> 
        </td>
        <td>
           <p>Email : <?php echo $user->email; ?></p> 
        </td>
        <td>
           <p>Number : <?php echo $user->number; ?></p> 
        </td>
	 </tr> <!--END invite info row-->
    </table><!--END containing table-->
  </div>
</body>
</html>