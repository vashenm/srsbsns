<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SRSBSNS</title>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="row">
                <div class="col-sm-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal" id="form">
                                <div class="messages">
                                </div>
                                <div class="form-group">
                                    <input class="form-control name" type="text" name="name" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <input class="form-control surname" type="text" name="surname" placeholder="Surname">
                                </div>
                                <div class="form-group">
                                    <input class="form-control email" type="email" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input class="form-control number" type="text" name="number" placeholder="Number">
                                </div>
                                <div class="form-group">
                                    <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}"></div>
                                </div>
                                <!-- Button -->
                                <div class="form-group">
                                    <button type="button" onclick="save()" class="btn btn-primary">Send <span class="fa fa-paper-plane-o"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        $(document).ready(function() {
        $('form').on('submit', function(e){
            console.log('das');
            e.preventDefault();
        });
        });

        $url = {!! json_encode($url) !!};
        function save() {
            //console.log($("iframe").contents().find("#recaptcha-accessible-status"));
            var data = new FormData();
		    data.append('name', $('.name')[0].value);
            data.append('surname', $('.surname')[0].value);
            data.append('number', $('.number')[0].value);
            data.append('email', $('.email')[0].value);
            data.append('g-recaptcha-response', grecaptcha.getResponse());
            var xhr = new XMLHttpRequest();
            xhr.open('POST', $url+'/contact', true);
            xhr.onload = function () {
                var obj = JSON.parse(xhr.response);
                if(obj.status === 'error'){
                    $('.errorul').remove();
                    console.log(obj.message);
                    $errors = '<ul class="mt-3 errorul">';
                    for (var key in obj.message) {
                        $errors = $errors+'<li>'+obj.message[key][0]+'</li>';
                    }
                    console.log($errors);
                    $('.messages').append($errors+'</ul>');
                }
                
                if(obj.status === 'success'){
                    $('.errorul').remove();
                    $('.messages').append('<p>Submission successful</p><p><a href="'+$url+'/export" target="none">Please click here to export</a></p>');
                }
            };
            xhr.send(data);
        }
    </script>
</html>
